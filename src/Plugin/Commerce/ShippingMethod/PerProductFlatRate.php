<?php

namespace Drupal\commerce_shipping_per_product\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Per-product Flat Rate shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "shipping_per_product_flat_rate",
 *   label = @Translation("Per-product flat rate"),
 * )
 */
class PerProductFlatRate extends ShippingMethodBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PerProductFlatRate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager);

    $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_label' => NULL,
      'default_rate_amount' => NULL,
      'services' => ['default'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $amount = $this->configuration['default_rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => t('Rate label'),
      '#description' => t('Shown to customers during checkout.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];

    $form['default_rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => t('Default rate amount'),
      '#description' => t('Per-item rate used when neither a per-variation flat rate nor a per-product flat rate is defined.'),
      '#default_value' => $amount,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['default_rate_amount'] = $values['default_rate_amount'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    // Rate IDs aren't used in a flat rate scenario because there's always a
    // single rate per plugin, and there's no support for purchasing rates.

    // Default rate when neither per-variation nor per-product rates have been set.
    $default_rate = $this->configuration['default_rate_amount'];
    $total_rate = new Price(0, $default_rate['currency_code']);
    $default_rate = new Price($default_rate['number'], $default_rate['currency_code']);

    $order_item_storage = $this->entityTypeManager->getStorage('commerce_order_item');
    foreach ($shipment->getItems() as $shipment_item) {
      $rate = NULL;
      $orderItem = $order_item_storage->load($shipment_item->getOrderItemId());

      // Attempt to get per-variation flat rate.
      $variation = $orderItem->getPurchasedEntity();
      if ($variation->hasField('shipping_flat_rate')) {
        if (!$variation->get('shipping_flat_rate')->isEmpty()) {
          $rate = $variation->get('shipping_flat_rate')->first()->toPrice();
        }
      }

      // If no per-variation flat rate, attempt to get per-product flat rate.
      if (!isset($rate)) {
        $product = $variation->getProduct();
        if ($product->hasField('shipping_flat_rate')) {
          if (!$product->get('shipping_flat_rate')->isEmpty()) {
            $rate = $product->get('shipping_flat_rate')->first()->toPrice();
          }
        }
      }

      // Fallback to the plugin default flat rate.
      if (!isset($rate)) {
        $rate = $default_rate;
      }

      $rate = $rate->multiply((string) $shipment_item->getQuantity());
      $total_rate = $total_rate->add($rate);
    }

    $rate_id = 0;
    $rates = [];
    $rates[] = new ShippingRate($rate_id, $this->services['default'], $total_rate);

    return $rates;
  }

}
